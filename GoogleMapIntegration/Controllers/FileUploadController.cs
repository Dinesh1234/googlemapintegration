﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using GoogleMapIntegration.Models;

namespace GoogleMapIntegration.Controllers
{
    public class FileUploadController : Controller
    {
        //
        // GET: /FileUpload/

        TourAndTravelEntitiesDataResults dbcontext = new TourAndTravelEntitiesDataResults();

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult UploadExcel(HttpPostedFileBase FileUpload)
        {
            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                if (FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    
                    string filename = FileUpload.FileName;
                    string targetpath = System.Web.Configuration.WebConfigurationManager.AppSettings["myFilePath"].ToString();
                    string newtargetpath = targetpath.Replace(@"\\", @"\");
                    string filepath = Path.Combine(newtargetpath, filename);
                    FileUpload.SaveAs(filepath);
               
                    DataSet datasetData = ReadExcelFile(filepath);
                   
                    if (datasetData != null)
                    {
                        SaveEmployeeDetails(datasetData);
                    }


                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    data.Add("Please upload a valid excel file of version 2007 and above");
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (FileUpload == null) data.Add("You uploaded an empty file. Please choose a valid excel file");
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        private DataSet ReadExcelFile(string filepath)
        {
            DataSet Ds = new DataSet();

            try
            {
                //Open the Excel file in Read Mode using OpenXml.
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filepath, false))
                {
                    for (int i = 0; i < spreadSheetDocument.WorkbookPart.Workbook.Sheets.Count(); i++)
                    {
                        DataTable dt = new DataTable();
                        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;

                        //Read the first Sheet from Excel file.
                        Sheet sheets = (Sheet)(spreadSheetDocument.WorkbookPart.Workbook.Sheets.ChildElements[i]);

                        dt.TableName = sheets.Name;

                        string relationshipId = sheets.Id.Value;

                        //Get the Worksheet instance.
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);

                        if (worksheetPart != null)
                        {
                            Worksheet workSheet = worksheetPart.Worksheet;
                            SheetData sheetData = workSheet.GetFirstChild<SheetData>();

                            //Fetch all the rows present in the Worksheet.
                            IEnumerable<Row> rows = sheetData.Descendants<Row>();

                            // This will set the header in Datatable
                            foreach (Cell cell in rows.ElementAt(0))
                            {
                                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                            }

                            foreach (Row row in rows) //This foreach loop will also include header row...
                            {
                                DataRow tempRow = dt.NewRow();
                                int columnIndex = 0;
                                foreach (Cell cell in row.Descendants<Cell>())
                                {
                                    // Gets the column index of the cell with data
                                    int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));

                                    //zero based index
                                    cellColumnIndex--;

                                    if (columnIndex < cellColumnIndex)
                                    {
                                        do
                                        {
                                            tempRow[columnIndex] = ""; //Insert blank data here;
                                            columnIndex++;
                                        }
                                        while (columnIndex < cellColumnIndex);
                                    }
                                    tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                    columnIndex++;
                                }
                                //Add rows to DataTable.
                                dt.Rows.Add(tempRow);
                            }
                            //...so remove the duplicate header row here.
                            dt.Rows.RemoveAt(0);
                        }
                        //Add single sheet data in dt
                        Ds.Tables.Add(dt);
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //for multiple sheets
            return Ds;
        }

        public bool SaveEmployeeDetails(DataSet dsdata)
        {        
            // iterate over your data set
            for (int dsIndex = 0; dsIndex < dsdata.Tables.Count; dsIndex++)
            {
                for (int i = 0; i < dsdata.Tables[dsIndex].Rows.Count; i++)
                {
                    using (var context = new TourAndTravelEntitiesDataResults())
                    {
                        if (dsdata.Tables[dsIndex].TableName == "countryTable")
                        {
                            var countrylist = new Country();
                            countrylist.CountryName = dsdata.Tables[dsIndex].Rows[i][0].ToString();
                            countrylist.Longitude = dsdata.Tables[dsIndex].Rows[i][1].ToString();
                            countrylist.Lattitude = dsdata.Tables[dsIndex].Rows[i][2].ToString();
                            context.Countries.Add(countrylist);
                            context.SaveChanges();
                        }
                        else if (dsdata.Tables[dsIndex].TableName == "cityTable")
                        {
                            var citylist = new City();
                            citylist.CityName = dsdata.Tables[dsIndex].Rows[i][0].ToString();
                            citylist.CountryId = Convert.ToInt32(dsdata.Tables[dsIndex].Rows[i][1].ToString());
                            citylist.Longitude = dsdata.Tables[dsIndex].Rows[i][2].ToString();
                            citylist.Lattitude = dsdata.Tables[dsIndex].Rows[i][3].ToString();
                            context.Cities.Add(citylist);
                            context.SaveChanges();
                        }
                        else
                        {
                            var userlist = new GoogleMapIntegration.Models.UserInfo();
                            userlist.UserName = dsdata.Tables[dsIndex].Rows[i][0].ToString();
                            userlist.CountryId = Convert.ToInt32(dsdata.Tables[dsIndex].Rows[i][1].ToString());
                            userlist.CityId = Convert.ToInt32(dsdata.Tables[dsIndex].Rows[i][2].ToString());
                            userlist.VenderName = dsdata.Tables[dsIndex].Rows[i][3].ToString();
                            userlist.PermamentAddress = dsdata.Tables[dsIndex].Rows[i][4].ToString();
                            userlist.TourExpences = dsdata.Tables[dsIndex].Rows[i][5].ToString();
                            context.UserInfoes.Add(userlist);
                            context.SaveChanges();
                        }
                    }
                }
            }
          
            return true;
        }

        private string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return "";
            }
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && cell.DataType == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        private int? GetColumnIndexFromName(string columnName)
        {
            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, obj);
                return memoryStream.ToArray();
            }
        }

        private string GetColumnName(string cellReference)
        {           
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }
    }
}
