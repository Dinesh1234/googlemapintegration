﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoogleMapIntegration.Models;

namespace GoogleMapIntegration.Controllers
{
    public class MapController : Controller
    {
        TourAndTravelEntitiesDataResults dbcontext = new TourAndTravelEntitiesDataResults();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetMapDataByParameter(string CountryName)
        {
            int countryid = dbcontext.Countries.Where(x => (x.CountryName).ToUpper() ==(CountryName).ToUpper()).Select(x => x.CountryId).FirstOrDefault();
            var data = dbcontext.Sp_getUserdataByCountry(countryid).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
