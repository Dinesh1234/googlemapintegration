//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoogleMapIntegration.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class City
    {
        public City()
        {
            this.UserInfoes = new HashSet<UserInfo>();
        }
    
        public int CityId { get; set; }
        public string CityName { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
    
        public virtual Country Country { get; set; }
        public virtual ICollection<UserInfo> UserInfoes { get; set; }
    }
}
