//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoogleMapIntegration.Models
{
    using System;
    
    public partial class Sp_getUserdataByCountry_Result
    {
        public string UserName { get; set; }
        public string VenderName { get; set; }
        public string PermamentAddress { get; set; }
        public string TourExpences { get; set; }
        public string CountryLongitude { get; set; }
        public string CountryLattitude { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string CityLognitude { get; set; }
        public string CityLattitude { get; set; }
    }
}
